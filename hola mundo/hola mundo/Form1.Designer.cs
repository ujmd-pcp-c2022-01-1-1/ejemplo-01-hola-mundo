﻿
namespace hola_mundo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnhola = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnhola
            // 
            this.btnhola.Location = new System.Drawing.Point(28, 35);
            this.btnhola.Name = "btnhola";
            this.btnhola.Size = new System.Drawing.Size(119, 49);
            this.btnhola.TabIndex = 0;
            this.btnhola.Text = "HOLA";
            this.btnhola.UseVisualStyleBackColor = true;
            this.btnhola.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(239, 35);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(119, 49);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "Adios";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(393, 118);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btnhola);
            this.Name = "Form1";
            this.Text = "HOLA MUNDO";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnhola;
        private System.Windows.Forms.Button btn2;
    }
}

