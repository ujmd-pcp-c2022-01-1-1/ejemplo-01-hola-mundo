﻿
namespace calculadora02
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button btn6;
            this.btnlimpiar = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.pantalla = new System.Windows.Forms.TextBox();
            this.btn0 = new System.Windows.Forms.Button();
            this.btndivision = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btnmultiplicacion = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btnpunto = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btnresta = new System.Windows.Forms.Button();
            this.btnigual = new System.Windows.Forms.Button();
            this.btnraiz = new System.Windows.Forms.Button();
            this.btnsuma = new System.Windows.Forms.Button();
            btn6 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.Location = new System.Drawing.Point(34, 56);
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.Size = new System.Drawing.Size(45, 35);
            this.btnlimpiar.TabIndex = 0;
            this.btnlimpiar.Text = "CE";
            this.btnlimpiar.UseVisualStyleBackColor = true;
            this.btnlimpiar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(34, 179);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(45, 35);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(34, 138);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(45, 35);
            this.btn4.TabIndex = 2;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(34, 97);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(45, 35);
            this.btn7.TabIndex = 3;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // pantalla
            // 
            this.pantalla.Location = new System.Drawing.Point(34, 30);
            this.pantalla.Name = "pantalla";
            this.pantalla.ReadOnly = true;
            this.pantalla.Size = new System.Drawing.Size(208, 20);
            this.pantalla.TabIndex = 12;
            this.pantalla.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.pantalla.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(34, 220);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(96, 35);
            this.btn0.TabIndex = 1;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btndivision
            // 
            this.btndivision.Location = new System.Drawing.Point(85, 56);
            this.btndivision.Name = "btndivision";
            this.btndivision.Size = new System.Drawing.Size(45, 35);
            this.btndivision.TabIndex = 0;
            this.btndivision.Text = "/";
            this.btndivision.UseVisualStyleBackColor = true;
            this.btndivision.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(85, 179);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(45, 35);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(85, 97);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(45, 35);
            this.btn8.TabIndex = 3;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btnmultiplicacion
            // 
            this.btnmultiplicacion.Location = new System.Drawing.Point(136, 56);
            this.btnmultiplicacion.Name = "btnmultiplicacion";
            this.btnmultiplicacion.Size = new System.Drawing.Size(45, 35);
            this.btnmultiplicacion.TabIndex = 0;
            this.btnmultiplicacion.Text = "*";
            this.btnmultiplicacion.UseVisualStyleBackColor = true;
            this.btnmultiplicacion.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(136, 179);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(45, 35);
            this.btn3.TabIndex = 1;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btnpunto
            // 
            this.btnpunto.Location = new System.Drawing.Point(136, 220);
            this.btnpunto.Name = "btnpunto";
            this.btnpunto.Size = new System.Drawing.Size(45, 35);
            this.btnpunto.TabIndex = 1;
            this.btnpunto.Text = ".";
            this.btnpunto.UseVisualStyleBackColor = true;
            this.btnpunto.Click += new System.EventHandler(this.button21_Click);
            // 
            // btn6
            // 
            btn6.Location = new System.Drawing.Point(136, 138);
            btn6.Name = "btn6";
            btn6.Size = new System.Drawing.Size(45, 35);
            btn6.TabIndex = 2;
            btn6.Text = "6";
            btn6.UseVisualStyleBackColor = true;
            btn6.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(136, 97);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(45, 35);
            this.btn9.TabIndex = 3;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btnresta
            // 
            this.btnresta.Location = new System.Drawing.Point(187, 56);
            this.btnresta.Name = "btnresta";
            this.btnresta.Size = new System.Drawing.Size(45, 35);
            this.btnresta.TabIndex = 0;
            this.btnresta.Text = "-";
            this.btnresta.UseVisualStyleBackColor = true;
            this.btnresta.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnigual
            // 
            this.btnigual.Location = new System.Drawing.Point(187, 179);
            this.btnigual.Name = "btnigual";
            this.btnigual.Size = new System.Drawing.Size(45, 76);
            this.btnigual.TabIndex = 1;
            this.btnigual.Text = "=";
            this.btnigual.UseVisualStyleBackColor = true;
            // 
            // btnraiz
            // 
            this.btnraiz.Location = new System.Drawing.Point(187, 138);
            this.btnraiz.Name = "btnraiz";
            this.btnraiz.Size = new System.Drawing.Size(45, 35);
            this.btnraiz.TabIndex = 2;
            this.btnraiz.Text = "Raiz";
            this.btnraiz.UseVisualStyleBackColor = true;
            this.btnraiz.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnsuma
            // 
            this.btnsuma.Location = new System.Drawing.Point(187, 97);
            this.btnsuma.Name = "btnsuma";
            this.btnsuma.Size = new System.Drawing.Size(45, 35);
            this.btnsuma.TabIndex = 3;
            this.btnsuma.Text = "+";
            this.btnsuma.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 275);
            this.Controls.Add(this.pantalla);
            this.Controls.Add(this.btnsuma);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btnraiz);
            this.Controls.Add(this.btn7);
            this.Controls.Add(btn6);
            this.Controls.Add(this.btnigual);
            this.Controls.Add(this.btnpunto);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btnresta);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btnmultiplicacion);
            this.Controls.Add(this.btndivision);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btnlimpiar);
            this.Name = "Form1";
            this.Text = "CALCULADORA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnlimpiar;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.TextBox pantalla;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btndivision;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btnmultiplicacion;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btnpunto;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btnresta;
        private System.Windows.Forms.Button btnigual;
        private System.Windows.Forms.Button btnraiz;
        private System.Windows.Forms.Button btnsuma;
    }
}

