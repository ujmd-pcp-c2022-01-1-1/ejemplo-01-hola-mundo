﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hola_Mundo_PCP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola Mundo 8";
            //label1.ForeColor = Color.FromArgb(255,80,1);
            label1.ForeColor = Color.FromArgb(0xE15001);  // Se ocupó la forma en hexa mejor
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós Mundo 8";
            label1.ForeColor = Color.Black;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
